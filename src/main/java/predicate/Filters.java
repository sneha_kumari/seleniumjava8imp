package predicate;

import org.openqa.selenium.WebElement;

import java.util.function.Predicate;

public class Filters {

//    public static final Predicate<WebElement> STARTWITH=s->s.getText().startsWith("M");
    public static final Predicate<WebElement> STARTWITH=new Predicate<WebElement>() {
    @Override
    public boolean test(WebElement s) {
        return s.getText().startsWith("M");
    }
};
    public static final Predicate<WebElement> CONTAINS=c->{
        return (!c.getText().toLowerCase().contains("t"));
    };
    public static final Predicate<WebElement> HASLENGTH=l->{
        return l.getText().length()==5;
    };
}
